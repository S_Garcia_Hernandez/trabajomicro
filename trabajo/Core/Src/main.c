/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "fonts.h"
#include "test.h"
#include "ssd1306.h"
#include "stdio.h"
#include <string.h>
#include "stdbool.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/*CONEXIONADO DEL KEYPAD*/

#define R1_PORT GPIOB
#define R1_PIN GPIO_PIN_1

#define R2_PORT GPIOB
#define R2_PIN GPIO_PIN_2

#define R3_PORT GPIOE
#define R3_PIN GPIO_PIN_7

#define R4_PORT GPIOE
#define R4_PIN GPIO_PIN_8

#define C1_PORT GPIOC
#define C1_PIN GPIO_PIN_4

#define C2_PORT GPIOC
#define C2_PIN GPIO_PIN_5

#define C3_PORT GPIOB
#define C3_PIN GPIO_PIN_0



/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim6;

UART_HandleTypeDef huart6;
DMA_HandleTypeDef hdma_usart6_rx;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM6_Init(void);
static void MX_USART6_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/*Variable auxiliar para el control de acceso al sistema mediante contraseña y keypad*/

bool auxiliar=0;

/*VARIABLES VOLATIL PARA GESTIÓN DE LA INTERRUPCIÓN POR BOTÓN */
volatile int boton=0;

/*BUFFER PARA COMUNICACIÓN BLUETOOTH*/
char readBuf[6]="000000";

/*VARIABLES AUXILIARES PARA EL CALCULO DE TEMPERATURA Y RH:HUMEDAD*/
uint8_t Rh_byte1,Rh_byte2, Temp_byte1, Temp_byte2;
uint16_t SUM,RH,TEMP;

float temperatura=0;
float humedad=0;
uint8_t presence=0;

/*VARIABLES PARA CAD*/
uint32_t adcvalue[1], adcbuffer[1];

/*VARIABLES PARA EL GOBIERNO DEL KEYPAD*/

char TECLA;                         // almacena la tecla presionada
char CLAVE[7];                      // almacena en un array 6 digitos ingresados
char CLAVE_MAESTRA[7] = "123456";   // almacena en un array la contraseña maestra
int INDICE = 0;                    // indice del array


/*CALLBACK DE LECTURA POR INTERRUPCIÓN DEL VALOR DEL POTENCIOMETRO*/

void HAL_ADC_ConvCpltCallback (ADC_HandleTypeDef * hadc){

	if (hadc->Instance == ADC1){
		adcvalue[0]=adcbuffer[0];
	}
}

/*CALLBACK DE INTERRUPCIÓN POR PULSACIÓN DEL BOTÓN*/

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	if (GPIO_Pin==GPIO_PIN_0)
	{
			boton=1;

	}
}

/*Función para la lectura de la tecla del keypad pulsada*/

char read_keypad (void)
{
	/*ANTIREBOTES*/
	/*Para las filas(salidas) se usa una resistencia PULLDOWN de 10K
	 y para las entradas(columnas) se configura una PULLUP por software mediante el IDE*/
	HAL_Delay(400);
	 //Make ROW 1 LOW and all other ROWs HIGH
	HAL_GPIO_WritePin (R1_PORT, R1_PIN, GPIO_PIN_RESET);  //Pull the R1 low
	HAL_GPIO_WritePin (R2_PORT, R2_PIN, GPIO_PIN_SET);    // Pull the R2 High
	HAL_GPIO_WritePin (R3_PORT, R3_PIN, GPIO_PIN_SET);    // Pull the R3 High
	HAL_GPIO_WritePin (R4_PORT, R4_PIN, GPIO_PIN_SET);    // Pull the R4 High

	if (!(HAL_GPIO_ReadPin (C1_PORT, C1_PIN)))   // if the Col 1 is low
	{
		while (!(HAL_GPIO_ReadPin (C1_PORT, C1_PIN)));   // wait till the button is pressed
		return '1';
	}

	if (!(HAL_GPIO_ReadPin (C2_PORT, C2_PIN)))   // if the Col 2 is low
	{
		while (!(HAL_GPIO_ReadPin (C2_PORT, C2_PIN)));   // wait till the button is pressed
		return '2';
	}

	if (!(HAL_GPIO_ReadPin (C3_PORT, C3_PIN)))   // if the Col 3 is low
	{
		while (!(HAL_GPIO_ReadPin (C3_PORT, C3_PIN)));   // wait till the button is pressed
		return '3';
	}



	// Make ROW 2 LOW and all other ROWs HIGH
	HAL_GPIO_WritePin (R1_PORT, R1_PIN, GPIO_PIN_SET);  //Pull the R1 low
	HAL_GPIO_WritePin (R2_PORT, R2_PIN, GPIO_PIN_RESET);  // Pull the R2 High
	HAL_GPIO_WritePin (R3_PORT, R3_PIN, GPIO_PIN_SET);  // Pull the R3 High
	HAL_GPIO_WritePin (R4_PORT, R4_PIN, GPIO_PIN_SET);  // Pull the R4 High

	if (!(HAL_GPIO_ReadPin (C1_PORT, C1_PIN)))   // if the Col 1 is low
	{
		while (!(HAL_GPIO_ReadPin (C1_PORT, C1_PIN)));   // wait till the button is pressed
		return '4';
	}

	if (!(HAL_GPIO_ReadPin (C2_PORT, C2_PIN)))   // if the Col 2 is low
	{
		while (!(HAL_GPIO_ReadPin (C2_PORT, C2_PIN)));   // wait till the button is pressed
		return '5';
	}

	if (!(HAL_GPIO_ReadPin (C3_PORT, C3_PIN)))   // if the Col 3 is low
	{
		while (!(HAL_GPIO_ReadPin (C3_PORT, C3_PIN)));   // wait till the button is pressed
		return '6';
	}


	 //Make ROW 3 LOW and all other ROWs HIGH
	HAL_GPIO_WritePin (R1_PORT, R1_PIN, GPIO_PIN_SET);  //Pull the R1 low
	HAL_GPIO_WritePin (R2_PORT, R2_PIN, GPIO_PIN_SET);  // Pull the R2 High
	HAL_GPIO_WritePin (R3_PORT, R3_PIN, GPIO_PIN_RESET);  // Pull the R3 High
	HAL_GPIO_WritePin (R4_PORT, R4_PIN, GPIO_PIN_SET);  // Pull the R4 High

	if (!(HAL_GPIO_ReadPin (C1_PORT, C1_PIN)))   // if the Col 1 is low
	{
		while (!(HAL_GPIO_ReadPin (C1_PORT, C1_PIN)));   // wait till the button is pressed
		return '7';
	}

	if (!(HAL_GPIO_ReadPin (C2_PORT, C2_PIN)))   // if the Col 2 is low
	{
		while (!(HAL_GPIO_ReadPin (C2_PORT, C2_PIN)));   // wait till the button is pressed
		return '8';
	}

	if (!(HAL_GPIO_ReadPin (C3_PORT, C3_PIN)))   // if the Col 3 is low
	{
		while (!(HAL_GPIO_ReadPin (C3_PORT, C3_PIN)));   // wait till the button is pressed
		return '9';
	}



	 //Make ROW 4 LOW and all other ROWs HIGH
	HAL_GPIO_WritePin (R1_PORT, R1_PIN, GPIO_PIN_SET);  //Pull the R1 low
	HAL_GPIO_WritePin (R2_PORT, R2_PIN, GPIO_PIN_SET);  // Pull the R2 High
	HAL_GPIO_WritePin (R3_PORT, R3_PIN, GPIO_PIN_SET);  // Pull the R3 High
	HAL_GPIO_WritePin (R4_PORT, R4_PIN, GPIO_PIN_RESET);  // Pull the R4 High

	if (!(HAL_GPIO_ReadPin (C1_PORT, C1_PIN)))   // if the Col 1 is low
	{
		while (!(HAL_GPIO_ReadPin (C1_PORT, C1_PIN)));   // wait till the button is pressed
		return '*';
	}

	if (!(HAL_GPIO_ReadPin (C2_PORT, C2_PIN)))   // if the Col 2 is low
	{
		while (!(HAL_GPIO_ReadPin (C2_PORT, C2_PIN)));   // wait till the button is pressed
		return '0';
	}

	if (!(HAL_GPIO_ReadPin (C3_PORT, C3_PIN)))   // if the Col 3 is low
	{
		while (!(HAL_GPIO_ReadPin (C3_PORT, C3_PIN)));   // wait till the button is pressed
		return '#';
	}


}

/*FUNCIÓN ANTIRREBOTES. DEVUELVE TRUE DESPUÉS DE UN TIEMPO(PARA SUPERAR LOS REBOTES)*/

//puntero a int que llamo boton;luego a la función se le pasa la dirección de botón (&boton)
int debouncer(volatile int *boton, GPIO_TypeDef* GPIO_port, uint16_t GPIO_number){
	static uint8_t button_count=0;
	static int contadordetiempo=0;

	if (*boton==1){
		if (button_count==0) {
			contadordetiempo=HAL_GetTick();
			button_count++;
		}
		if (HAL_GetTick()-contadordetiempo>=20){//cada vez que pasan 20ms
			contadordetiempo=HAL_GetTick();
			if (HAL_GPIO_ReadPin(GPIO_port, GPIO_number)!=1){//cada 20ms miro el valor del boton
				button_count=1;
			}
			else{
				button_count++;
			}
			//Periodo antirebotes
			if (button_count==4){ //si el contador del boton llega a 4 ya se ha estabiizado la señal
				button_count=0;
				*boton=0;//fin interrupción
				return 1;
			}
		}
	}
	return 0;
}

						/* COMUNICACIÓN MODULO HC-05*/

/*Si los mensajes llegan por el virtual port se llama a esta función*/
void CDC_ReceiveCallBack(uint8_t *buf, uint32_t len){
    //me llegan los datos en un puntero y su tamaño//
	CDC_Transmit_FS(buf, len);//reescribe en puerto serie
	//Lo transmito directamente por UART
	HAL_UART_Transmit(&huart6, buf, len, HAL_MAX_DELAY);
}

//callback de las interrupciones de la comunicación UART (HC-05 -> STM32)//

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle) {
	/*Lo que llega por UART, lo reenvia el modulo HC-05,
	 ya que previamente le ha llegado a el por bluetooth*/
	CDC_Transmit_FS(readBuf, 6);//El micro manda lo recibido por UART a traves del puerto serie
	HAL_UART_Receive_IT(&huart6, (uint8_t*)readBuf, 6);//vuelvo a llamar a la interrupción
}

				/*FIN COMUNICACIÓN MODULO HC-05*/

				/*FUNCIONES PARA LECTURA DE DATOS DEL SENSOR DHT22*/

void delay(uint16_t time){
	__HAL_TIM_SET_COUNTER(&htim6,0);
	while((__HAL_TIM_GET_COUNTER(&htim6))<time);
}

void Display_Temp (float Temp,float rh)
{
	char str[20] = {0};
	SSD1306_GotoXY (0,0);
	sprintf (str, "TEM(C):%.2f", Temp);
	SSD1306_Puts (str, &Font_11x18, 1);

    char str2[20] = {0};
	SSD1306_GotoXY (0, 30);

	sprintf (str2, "HUM(/):%.2f", rh);
	SSD1306_Puts (str2, &Font_11x18, 1);
    SSD1306_UpdateScreen();

}

void Set_Pin_Output (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}

void Set_Pin_Input (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}

#define DHT22_PORT GPIOA
#define DHT22_PIN GPIO_PIN_1

void DHT22_Start (void)
{
	/*se establece el pin PA1 como salida para que el micro pueda escribir en él */
	Set_Pin_Output(DHT22_PORT, DHT22_PIN);
	/* Nivel de comienzo de comunicación */
	HAL_GPIO_WritePin(DHT22_PORT, DHT22_PIN, 1);
	delay(1000);
	/* El micro debe enviar un nivel bajo durante 18 μs */
	HAL_GPIO_WritePin(DHT22_PORT, DHT22_PIN, 0);
	delay (18000);
	/* El micro pone la señal a nivel alto y tras un tiempo de transición*/
	HAL_GPIO_WritePin(DHT22_PORT, DHT22_PIN, 1);
	delay(20);
	/*Se establece el pin PA1 como entrada*/
	Set_Pin_Input(DHT22_PORT, DHT22_PIN);
}

uint8_t DHT22_Check_Response (void)
{
	/*Se establece el pin PA1 como entrada*/
	Set_Pin_Input(DHT22_PORT, DHT22_PIN);
	uint8_t Response = 0;
	/* El micro debe esperar 40 μs la respuesta del sensor */
	delay (40);
	/* if the pin is low:Protocolo de comunicación detallado en datashet del sensor*/
	if (!(HAL_GPIO_ReadPin (DHT22_PORT, DHT22_PIN)))
	{
		delay (80);   // wait for 80us

		if ((HAL_GPIO_ReadPin (DHT22_PORT, DHT22_PIN))) Response = 1;  // if the pin is high, response is ok
		else Response = -1;
	}

	while ((HAL_GPIO_ReadPin (DHT22_PORT, DHT22_PIN)));   // wait for the pin to go low
	return Response;
}

uint8_t DHT22_Read (void)
{
	/*ya se ha establecido la conexión, empieza el envio de bits con la info de Temperatura Humedad */

	uint8_t i,j;
	for (j=0;j<8;j++)
	{
		while (!(HAL_GPIO_ReadPin (DHT22_PORT, DHT22_PIN)));   // wait for the pin to go high
		delay (40);   // wait for 40 us

		if (!(HAL_GPIO_ReadPin (DHT22_PORT, DHT22_PIN)))   // if the pin is low
		{
			i&= ~(1<<(7-j));   // write 0
		}
		else i|= (1<<(7-j));  // if the pin is high, write 1
		while ((HAL_GPIO_ReadPin (DHT22_PORT, DHT22_PIN)));  // wait for the pin to go low
	}

	return i;
}

		/*FIN DE FUNCIONES PARA LECTURA DE DATOS DEL SENSOR DHT22*/

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  MX_TIM6_Init();
  MX_USART6_UART_Init();
  MX_USB_DEVICE_Init();
  MX_TIM1_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
  HAL_UART_Receive_IT(&huart6, (uint8_t*)readBuf, 6);

  //INICIALIZACIÓN DEL SISTEMA//

  SSD1306_Init();
  SSD1306_Clear();
  SSD1306_GotoXY (0,0);
  SSD1306_Puts ("BIENVENIDO", &Font_11x18, 1);
  SSD1306_GotoXY (10, 30);
  SSD1306_Puts ("AL SISTEMA", &Font_11x18, 1);
  SSD1306_UpdateScreen();
  HAL_Delay (2000);
  SSD1306_ScrollRight(0,7);  // scroll entire screen
  HAL_Delay(2000);  // 2 seconds
  SSD1306_ScrollLeft(0,7);  // scroll entire screen
  HAL_Delay(2000);
  SSD1306_Stopscroll();
												  /* TESTS DE PANTALLA OLED */
												  /* TestLines(1);
													HAL_Delay (1000);
												   TestRectangles(1);
												  HAL_Delay (1000);
													TestFilledRectangles(1);
												   HAL_Delay (1000);
												   TestCircles(6,1);
													HAL_Delay (1000);
													TestFilledCircles(6, 1);
												   TestTriangles(1);
												   SSD1306_UpdateScreen();*/

  HAL_TIM_Base_Start(&htim6);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
  HAL_ADC_Start_DMA(&hadc1, adcbuffer, 1);

  //ACCESO CON CONTRASEÑA AL SISTEMA//

  SSD1306_Clear();
  SSD1306_GotoXY (0,0);
  SSD1306_Puts ("PIN :", &Font_11x18, 1);
  SSD1306_UpdateScreen();
  HAL_Delay(500);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
    /* USER CODE END WHILE */
    /* USER CODE BEGIN 3 */
	  while(auxiliar==0){
		  TECLA = read_keypad(); // obtiene tecla presionada y asigna a variable
		  if (TECLA!=0x01){      //Sí no se esta pulsando ninguna tecla no cuenta
		     CLAVE[INDICE] = TECLA;                 // almacena en array la tecla presionada
		     INDICE++;                              // incrementa indice en uno
		     SSD1306_GotoXY (30+INDICE*10,30);      //situamos cursor en la pantalla oled
		     SSD1306_Puts (&TECLA, &Font_11x18, 1); //escribimos la tecla pulsada
			 SSD1306_UpdateScreen();                //actualizamos la pantalla
			 HAL_Delay(500);
		  }

		  if(INDICE == 6) {      // si ya se almacenaron los 6
		      if(!strcmp(CLAVE, CLAVE_MAESTRA)){  // compara clave ingresada con clave maestra
		    	  SSD1306_Clear();
		    	  SSD1306_GotoXY (0,0);
				  SSD1306_Puts ("Correcta", &Font_11x18, 1);
				  SSD1306_GotoXY (10, 30);
				  SSD1306_Puts ("BIENVENIDO", &Font_11x18, 1);
				  SSD1306_UpdateScreen();
				  HAL_Delay(800);
				  auxiliar=1;
				 INDICE=0;
		      }
		      else{
		    	  SSD1306_Clear();
				  SSD1306_GotoXY (0,0);
				  SSD1306_Puts ("Incorrecta", &Font_11x18, 1);
				  SSD1306_UpdateScreen();
				  HAL_Delay(1000);
				  SSD1306_Clear();
				  SSD1306_GotoXY (0,0);
			      SSD1306_Puts ("PIN :", &Font_11x18, 1);
				  SSD1306_UpdateScreen();
		          INDICE = 0;
		      }
		    }

	  }


	  /*Se comprueba si se ha pulsado el boton*/

	  if (debouncer(&boton, GPIOA, GPIO_PIN_0)){
		  SSD1306_Clear();
		  SSD1306_GotoXY (0,0);
		  SSD1306_Puts ("Alarma", &Font_11x18, 1);
		  SSD1306_GotoXY (10, 30);
		  SSD1306_Puts ("Activa", &Font_11x18, 1);
		  SSD1306_UpdateScreen();
		  //configuración buzzer con valor de un potenciometro
		  /*Temporizador a f=10 KHz*/
		  while(strcmp(readBuf,"FINALA")!=0){
			  __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, adcvalue[0]*0.00977); //Buzzer
			  if (adcvalue[0]>500)
			       HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 1);
		  }
		  SSD1306_Clear();
		  SSD1306_GotoXY (0,0);
		  SSD1306_Puts ("Alarma", &Font_11x18, 1);
		  SSD1306_GotoXY (10, 30);
		  SSD1306_Puts ("Desactivada", &Font_11x18, 1);
		  SSD1306_UpdateScreen(); //display
		}

	//LECTURA DEL PIN PA2(SENSOR PIR)

	  if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_2)){
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET);
		  SSD1306_Clear();
		  SSD1306_GotoXY (0,0);
		  SSD1306_Puts ("SE DETECTO", &Font_11x18, 1);
		  SSD1306_GotoXY (10, 30);
		  SSD1306_Puts ("PRESENCIA", &Font_11x18, 1);
		  SSD1306_UpdateScreen(); //display
		  SSD1306_ScrollLeft(0,9);  // scroll entire screen
		  HAL_Delay(2000);  // 2 seconds
		  SSD1306_ScrollRight(0,9);  // scroll entire screen
		  HAL_Delay(2000);  // 2 sec
		  SSD1306_Stopscroll();
		  while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_2)==1 && strcmp(readBuf,"000000")==0);

		  if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_2)==0){
			  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 0);
			  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, 0);
			  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, 0);
			  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, 0);
			  SSD1306_Clear();
			  SSD1306_GotoXY (0,0);
		      SSD1306_Puts ("NO HAY", &Font_11x18, 1);
		      SSD1306_GotoXY (10, 30);
		      SSD1306_Puts ("PRESENCIA", &Font_11x18, 1);
		      SSD1306_UpdateScreen(); //display
		  }

	  }

	  /* PROTOCOLO DE COMUNICACIÓN BLUETOOTH*/

	  while (strcmp(readBuf,"LEDSON")==0){
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 1);
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, 1);
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, 1);
			HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, 1);
			SSD1306_Clear();
			SSD1306_GotoXY (0,0);
			SSD1306_Puts ("LEDS", &Font_11x18, 1);
			SSD1306_GotoXY (10, 30);
			SSD1306_Puts ("ENCENDIDOS", &Font_11x18, 1);
			SSD1306_UpdateScreen();
			HAL_Delay (2000);
			SSD1306_ScrollRight(0,7);
			SSD1306_ScrollLeft(0,7);
			HAL_Delay(2000);
			SSD1306_Stopscroll();
			SSD1306_Clear();
	 }

	while(strcmp(readBuf,"LEDSOF")==0){
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, 0);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, 0);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, 0);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, 0);
		SSD1306_Clear();
		SSD1306_GotoXY (0,0);
		SSD1306_Puts ("LEDS", &Font_11x18, 1);
		SSD1306_GotoXY (10, 30);
		SSD1306_Puts ("APAGADOS", &Font_11x18, 1);
		SSD1306_UpdateScreen(); //display
		HAL_Delay (2000);
	}

	while(strcmp(readBuf,"SERVON")==0 || temperatura>25 ){
		SSD1306_Clear();
		SSD1306_GotoXY (0,0);
		SSD1306_Puts ("SERVO", &Font_11x18, 1);
		SSD1306_GotoXY (10, 30);
		SSD1306_Puts ("ON", &Font_11x18, 1);
		SSD1306_UpdateScreen();
		int i=25;
	    HAL_Delay(1000);
	    while(i<250){
		  htim1.Instance->CCR4=i;
		  i++;
		  HAL_Delay(100);
	   }
	}

	while(strcmp(readBuf,"TEMPER")==0 ){
		  DHT22_Start();
		  presence= DHT22_Check_Response();
		  Rh_byte1 = DHT22_Read ();
		  Rh_byte2 = DHT22_Read ();
		  Temp_byte1 = DHT22_Read ();
		  Temp_byte2 = DHT22_Read ();
		  SUM = DHT22_Read();

		  TEMP = ((Temp_byte1<<8)|Temp_byte2);
		  RH = ((Rh_byte1<<8)|Rh_byte2);

		  temperatura = (float) (TEMP/10.0);
		  humedad = (float) (RH/10.0);

		  Display_Temp(temperatura,humedad);
		  HAL_Delay(500);
	}

  }
}

  /* USER CODE END 3 */


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 72;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_10B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode =ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 719;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1999;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 720;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 10;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 50-1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 65535;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief USART6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART6_UART_Init(void)
{

  /* USER CODE BEGIN USART6_Init 0 */

  /* USER CODE END USART6_Init 0 */

  /* USER CODE BEGIN USART6_Init 1 */

  /* USER CODE END USART6_Init 1 */
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 9600;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART6_Init 2 */

  /* USER CODE END USART6_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
  /* DMA2_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1|GPIO_PIN_2, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PC4 PC5 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PB0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PB1 PB2 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PE7 PE8 */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PD12 PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
